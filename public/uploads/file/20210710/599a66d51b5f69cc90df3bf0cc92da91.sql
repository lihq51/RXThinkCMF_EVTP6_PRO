/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 100406
 Source Host           : localhost:3306
 Source Schema         : evtp6.pro

 Target Server Type    : MySQL
 Target Server Version : 100406
 File Encoding         : 65001

 Date: 10/07/2021 16:24:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for evt_file
-- ----------------------------
DROP TABLE IF EXISTS `evt_file`;
CREATE TABLE `evt_file`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名',
  `length` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件大小(KB)',
  `url` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件地址',
  `directory` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否文件夹：0文件夹 1文件',
  `type` tinyint(255) UNSIGNED NOT NULL DEFAULT 0 COMMENT '文件类型：1图片 2文件',
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级ID',
  `sort` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '排序',
  `create_user` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加人',
  `create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_user` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '修改人',
  `update_time` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '更新时间',
  `mark` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '有效标识：1正常 0删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件管理表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
